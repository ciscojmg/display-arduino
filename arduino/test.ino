unsigned long count = 0;

void setup()
{
    Serial.begin(9600);   
}

void loop()
{
    
    if ( count < 10 )
        Serial.print("0000");
    else if ( count < 100 )
        Serial.print("000");
    else if ( count < 1000 )
        Serial.print("00");
    else if ( count < 10000 )
        Serial.print("0");

    
    Serial.print(count);
    count = random(10);
    delay(2000);
}