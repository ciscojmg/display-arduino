# display-arduino

Display con arduino

* arduino
* node.js
  * socket.io
  * serialport
  * express

Para ejecutar el proyecto se debe tomar en consideracion el puerto serial por el cual se conecta el **arduino**, en este caso es el **COM3**. Modificar esa parte del codigo si cambia.

### Dependencias

Debe tener instalado [node.js](https://nodejs.org/en/)

* Entrar a la carpeta app `cd app/`
* Instalar dependencia `npm install`
* Ejecutar proyecto `node index.js`
* Ingresar a [localhost](http://localhost:3000/)

#### Multimedia
![peso2](/uploads/772d4bdaf628670ef55dc663e2b58a2f/peso2.gif)
